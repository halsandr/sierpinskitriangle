# Siepinski Triangle Generator

## What is it?
The chaos game is a method for generating the Sierpinski triangle fractal. It involves randomly selecting one of the vertices of a triangle and plotting a point halfway between the selected vertex and the current position of a "random walker." This process is repeated a large number of times, resulting in a pattern of points that approximates the Sierpinski triangle.

## How to use it?
Click 3 points to define the outer triangle, then click a point within to start the Siepinski generator.