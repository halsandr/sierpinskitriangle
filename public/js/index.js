export default class Sierpinski {
    /** @param {HTMLCanvasElement} canvasElement */
    constructor(canvasElement) {
        this.canvas = canvasElement;
        this.context = canvasElement.getContext("2d");
        this.trianglePoints = [];
        this.generateNum = 50000;
        
        this.init();
        window.addEventListener("resize", this.init);
        
    }
    
    init = () => {
        this.reset();
        this.canvas.width = this.canvas.parentElement.clientWidth;
        this.canvas.height = this.canvas.parentElement.clientHeight;
        this.context.fillStyle = "#000000";
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.trianglePoints = [];
        this.context.fillStyle = "#ffffff";
        this.pointSize = 3;
        this.startClickListener();
    }

    reset = () => {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    addTrianglePoint = (x, y) => {
        // console.log("Adding triangle point: ", x, y);
        
        this.drawTrianglePoint(x, y);

        // Needs a 4th point to start generating
        if(this.trianglePoints.length < 4) {
            this.trianglePoints.push({ x, y });
        }

        if(this.trianglePoints.length === 3) {
            // make internal points smaller
            this.pointSize = 1;
        }

        // If we have 4 points, remove the click listener and start generating
        if(this.trianglePoints.length === 4) {
            this.removeClickListener();
            this.generatePoints();
        }     
    }

    drawTrianglePoint = (x, y) => {
        if(this.pointSize === 1) {
            this.context.fillRect(x, y, 1, 1);
            return;
        }
        this.context.beginPath();
        this.context.arc(x, y, this.pointSize, 0, 2 * Math.PI);
        this.context.fill();
    }

    getPosition = (event) => {
        const rect = this.canvas.getBoundingClientRect();
        const x = event.clientX - rect.left;
        const y = event.clientY - rect.top;

        this.addTrianglePoint(Math.round(x), Math.round(y));
    }

    startClickListener = () => {
        this.removeClickListener();

        this.canvas.addEventListener('click', this.getPosition);
    }

    removeClickListener = () => {
        this.canvas.removeEventListener('click', this.getPosition);
    }

    sleep = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    generatePoints = async () => {
        // Most recent point is starting point
        let previousPoint = this.trianglePoints[3];
        for(let i = 0; i < this.generateNum; i++) {
            // Get a random point, excluding the last one
            const randomPoint = this.trianglePoints[Math.floor(Math.random() * (this.trianglePoints.length - 1))];

            // Calculate coordinates between the two points
            const x = (previousPoint.x + randomPoint.x) / 2;
            const y = (previousPoint.y + randomPoint.y) / 2;

            this.drawTrianglePoint( x, y );
            previousPoint = { x, y };

            // Slows down generation, but speeds up rendering as it goes
            if(i < 1000 || (i < 5000 && i % 10 === 0) || (i < 100000 && i % 100 === 0)) {
                await this.sleep(1);
            }
        }
    }
}